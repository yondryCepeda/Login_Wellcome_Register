package Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.jpa.config.Cascade;

@Entity
@Table(name = "Usuario")
@NamedQueries({
	@NamedQuery( name = Usuario.USUARIO_NOMBRE, query = "SELECT a FROM Usuario a WHERE a.nombre = :nombre")
	,
	@NamedQuery( name = Usuario.USUARIO_NOMBRE_CLAVE, query = "SELECT b FROM Usuario b WHERE b.nombre = :nombre AND b.clave = :clave")
})
public class Usuario implements Serializable{

	private static final long serialVersionUID = -6234104693209087269L;

	public static final String USUARIO_NOMBRE = "Usuario.usuariosRegistrados";
	public static final String USUARIO_NOMBRE_CLAVE = "Usuario.obtenerUsuarioFiltrado";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "clave")
	private String clave;
	
	@OneToMany(mappedBy = "usuario")
	private List<Celular> celular;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public List<Celular> getCelular() {
		return celular;
	}
	public void setCelular(List<Celular> celular) {
		this.celular = celular;
	}
	@Override
	public String toString() {
		return "Registro [id=" + id + ", nombre=" + nombre + ", clave=" + clave + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
