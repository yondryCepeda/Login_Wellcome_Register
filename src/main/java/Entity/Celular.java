package Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name = "celular")
@NamedQueries({
	@NamedQuery(name = Celular.OBTENER_NOMBRE_USUARIO, query = "SELECT a FROM Celular a Where a.usuario.nombre = :nombre"),
	@NamedQuery(name = Celular.OBTENER_ID_CELULAR, query = "SELECT b FROM Celular b Where b.id = :id"),
	@NamedQuery(name = Celular.OBTENER_NOMBRE_CELULAR, query = "SELECT c FROM Celular c Where c.nombre = :nombre")
})
public class Celular implements Serializable{
	private static final long serialVersionUID = 6016666034501906020L;

	public static final String OBTENER_NOMBRE_USUARIO = "Celular.ObtenerUsuarioCelular";
	
	public static final String OBTENER_ID_CELULAR = "Celular.ObtenerIdCelular";	
	
	public static final String OBTENER_NOMBRE_CELULAR = "Celular.ObtenerNombreCelular";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "PRECIO")
	private double precio;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USUARIO")
	private Usuario usuario;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		return "Celular [id=" + id + ", nombre=" + nombre + ", precio=" + precio + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Celular other = (Celular) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
}
