package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import Entity.Celular;
import Entity.Usuario;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable{

	private static final long serialVersionUID = -8169705414127895290L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();
	private int id;
	private String nombre;
	private String clave;
	List<Usuario> listaUsuarios = new ArrayList<Usuario>();
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}
	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	public void info(String type, String message) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,type,message));
	}
	
	public String login() {	
		
	System.out.println("nombre: "+nombre);
	System.out.println("clave: "+clave);
	Usuario usuario = new Usuario();
	listaUsuarios = em.createNamedQuery(Usuario.USUARIO_NOMBRE_CLAVE, Usuario.class)
				.setParameter("nombre", nombre)
				.setParameter("clave", clave)
				.getResultList();
		System.out.println("Lista de personas: "+listaUsuarios);
		usuario.setNombre(nombre);
		usuario.setClave(clave);
		usuario.setId(id);
		//condicion en caso de que el nombre de usuario o contraseņa no existan en la base de datos
		if(listaUsuarios.isEmpty()) {
			
			if(nombre == "") {
				info("WARNING", "Ingrese un nombre de usuario");
			}else if (clave == "") {
				info("WARNING", "Ingrese una clave");
			}else if(listaUsuarios.contains(usuario) != true){
			info("WARNING!", "nombre de usuario o clave incorrecto");
			}
			return "login.xhtml";
		
		}else {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", listaUsuarios.get(0));
			info("INFO", "se ha logeado correctamente");
			List<Celular> celular = em.createNamedQuery(Celular.OBTENER_NOMBRE_USUARIO, Celular.class)
					.setParameter("nombre", nombre)
					.getResultList();
			
			usuario.setCelular(celular);
			System.out.println("celulares para este usuario: " + usuario.getCelular());
			return "bienvenido?faces-redirect=true"; 	
			
		}
		
		
	}
	//este metodo se utiliza para cerrar la session con el metodo invalidate de HttpSession
	public String cerrar(){
		HttpSession hs = utils.Util.getSession();
		hs.invalidate();
		info("INFO", "Se ha cerrado la sesion");
		return "login";
}
	
	//este metodo se utiliza para que un usuario ya loggeado no pueda volver a loggearse sin antes cerrar la session
	public void usuarioRegistrado(){
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		if(us != null) {
			System.out.println("Usted ya esta logeado");
			info("INFO", "Usted ya esta loggeado");
			FacesContext.getCurrentInstance().getExternalContext().redirect("bienvenido.xhtml");
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	
	public String volver() {
		return "index";
	}
	
	//este metodo se utiliza para advertir a un usuario ya loggeado que no puede volver a loggearse sin antes cerrar la session
	public void verificarSession(){
		
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		if(us==null) {
			System.out.println("No tiene los permisos necesarios");
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	
}
