package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Entity.Usuario;
import Entity.Usuario;
@ManagedBean
@RequestScoped
public class RegistroBean implements Serializable{

	private static final long serialVersionUID = -5368251402164167970L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();
	
	
	List<Usuario> listaUsuario = new ArrayList<Usuario>();
	
	private int id;
	
	private String nombre;
	
	private String clave;
	
	private String clave2;

	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getClave2() {
		return clave2;
	}


	public void setClave2(String clave2) {
		this.clave2 = clave2;
	}
	
	
	public List<Usuario> getLista() {
		return listaUsuario;
	}


	public void setLista(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}


	public String getUsuario() {
		return nombre;
	}


	public void setUsuario(String nombre) {
		this.nombre = nombre;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}

	
	public String registrarUsuario() {
		
		String respuesta = "";
		Usuario usuario = new Usuario();
		usuario.setNombre(nombre);
		usuario.setClave(clave);
		listaUsuario = em.createNamedQuery(Usuario.USUARIO_NOMBRE, Usuario.class)
				.setParameter("nombre", nombre)
				.getResultList();

		System.out.println(listaUsuario);
	
		try {
			em.getTransaction().begin();
			
			if(listaUsuario.isEmpty() != true) {
				System.out.println("Este usuario ya existe!");
				info("Warning!","Este usuario ya esta existe!");
				respuesta = "index.xhtml";
				
				}else if(listaUsuario.isEmpty()){
		
					if(nombre == "") {
					System.out.println("Necesita un nombre de usuario");
					info("Warning!", "Necesita un nombre de usuario!");
					}
				else if(clave == "" || clave2 =="") {
					
					System.out.println("advertencia faltan campos por llenar");
					info("Warning!", "advertencia faltan campos por llenar");
				}
				else if(clave.equals(clave2) != true){
					
					System.out.println("sus contraseņas no coinciden");
					info("Warning!", "Sus contraseņas no coinciden");
				}else{
					em.persist(usuario);
					System.out.println("se ingresaron los datos correctamente!");
					respuesta = "login";
					info("Succeed!", "Se ingresaron los datos correctamente");
					
					}
					
				}
		em.getTransaction().commit();
		}catch (Exception e) {
			// TODO: handle exception
		}
		return respuesta;
		
		
	}
		
	public void info(String type, String message) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,type,message));
	}
	
	public String volver() {
		return "login";
	}
}
