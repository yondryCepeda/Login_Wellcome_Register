package Bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.oxm.mappings.BidirectionalPolicy;
import org.primefaces.context.RequestContext;

import Entity.Celular;
import Entity.Usuario;

@ManagedBean
@RequestScoped
public class CelularBean extends Celular implements Serializable{
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();

	private Celular celular;
	private String nombre; 
	private double precio;
	private Usuario usuario;
	List<Celular> listaCelular;
	
	
	public void setCelular(Celular celular) {
		this.celular = celular;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}	
	public Usuario getCelular() {
		return usuario;
	}
	public void setCelular(Usuario usuario) {
		this.usuario = usuario;
	}
	public void info(String type, String message) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,type,message));
	}
	
	public List<Celular> celularList(String nombreUsuario){	
		
		List<Celular> listaCelular = em.createNamedQuery(Celular.OBTENER_NOMBRE_USUARIO, Celular.class)
			.setParameter("nombre", nombreUsuario)
			.getResultList();		
	return listaCelular;
	}
	
		public List<Celular> idUsuario(int idUsuario) {
			
			Usuario r = em.find(Usuario.class, idUsuario);
			return r.getCelular();
			
		}
		
		public void nuevoCelular(int idUsuario){
			Celular c = new Celular();
			usuario = em.find(Usuario.class, idUsuario);
			c.setNombre(nombre);
			c.setPrecio(precio);
			c.setUsuario(usuario);
			System.out.println("nuevo celular: " + c);
			try {	
			listaCelular = em.createNamedQuery(Celular.OBTENER_NOMBRE_CELULAR, Celular.class)
					.setParameter("nombre", c.getNombre())
					.getResultList();
			
				em.getTransaction().begin();
				if(listaCelular.isEmpty()) {
					em.persist(c);
					info("Info!", "Succed.");
			        RequestContext.getCurrentInstance().execute("PF('dlgRegister').hide();");
				}else if(listaCelular.isEmpty() != true){
			        info("Warning!", "Celular ya existe!.");	      
				}
				em.getTransaction().commit();
			} catch (Exception e) {
				e.getMessage();
			}	
		}
		
	public void deleteCelular(int idCelular) {
		
		try {
			em.getTransaction().begin();
			em.clear();
			celular = em.find(Celular.class, idCelular);
			em.remove(celular);	
			info("Succeed","Celular Eliminado");
		} catch (Exception e) {
			// TODO: handle exception
		}
		em.getTransaction().commit();
		System.out.println("celular eliminado: " + celular);
	}
	
	public void limpiar() {	
		nombre = null;
		precio = 0;
	}
	public String detailsPage(int id){
		celular = em.find(Celular.class, id);
		
		System.out.println("celular encontrado: " + celular);
		 nombre = celular.getNombre();
		 precio = celular.getPrecio();
		 return "detail";
	}

	
	public void celularExistente() {
		Celular p = (Celular) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("celular");
	}
	public String volver() {
		return "bienvenido";
	}
	
}
